RESOLUTION = "FullHD"

FACES_DETECT = dict(
	detect_params=dict(scaleFactor=1.2, minNeighbors=5, minSize=(40, 40), maxSize=(160, 160))
)

TRACKING = dict(
	enable=False
)

RECOGNITION = dict(
	enable=False
)