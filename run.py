import argparse 
import os

import app.app


class CustomArgType(): 

    @staticmethod
    def capture(value): 
        try: 
            return int(value)
        except: 
            if not os.path.exists(value):
                raise Exception("Input video not exists")

            return value


    @staticmethod
    def existed_file(value): 
        if not os.path.exists(value): 
            raise Exception("File '{}' not exists".format(value))

        return value


def parse_args():
    parser = argparse.ArgumentParser(description="Script for real-time test face detector (Haar cascades)")

    parser.add_argument("--input", type=CustomArgType.capture, help="path to video file or number of a capture")
    parser.add_argument("--config", type=CustomArgType.existed_file, help="path to user config file (update default's config values)")
    parser.add_argument("-build-tracking", action="store_true", help="(re)build trackers cpp code after start")

    parser.set_defaults(input=0)

    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()

    app = app.app.App(args.input, user_config=vars(args).get("config", None))    

    app.run()

