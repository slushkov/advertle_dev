import cv2
import random

def show(frame, faces):
    AGE_FONT_PARAMS = dict(fontFace=cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, fontScale=2, thickness=3)

    for face in faces: 
        if "gender" in face["predictions"]:
            rect_color = (0, 0, 255) if face["predictions"]["gender"] == "male" else (255, 0, 0)
        else:
            rect_color = (0, 255, 0)
        
        if "age" in face["predictions"]:
            age_text = face["predictions"]["age"]
            text_size = cv2.getTextSize(age_text, **AGE_FONT_PARAMS)
            cv2.putText(frame, age_text, (face["rect"][0][0], face["rect"][0][1] - text_size[1]), color=rect_color, **AGE_FONT_PARAMS)

        # face = frame[face["rect"][0][0]:face["rect"][1][0], face["rect"][0][1]:face["rect"][1][1]]

        cv2.rectangle(frame, face["rect"][0], face["rect"][1], rect_color, 3)

    cv2.imshow("Camera", frame)
    cv2.waitKey(1)