from .primitives import Rect

class FacesBase(): 
    enable_capture_evristics = True

    def __init__(self, faces_merge_config):
        self.faces = []
        self.merge_config = faces_merge_config

    def foreach(self, statuses, func): 
        map(func, filter(lambda face: face.status in statuses, self.faces))

    def stat(self):
        return [f.stat() for f in self.faces]

    def track_active(self, frame, merge_intersected=True): 
        self.foreach(("captured", "tracked"), lambda face: face.track(frame))

        if self.merge_config.enable: 
            self.merge_intersected()

    def add_or_update_face(self, face): 
        if not self.enable_capture_evristics: 
            self.faces.append(face)
        else: 
            similar_face_num = self.__find_similar_face(face)

            if similar_face_num is None: 
                self.faces.append(face)
            else: 
                self.faces[similar_face_num].update(face)

    def __find_similar_face(self, face):
        for num, face_in_base in enumerate(self.faces): 
            if face_in_base.is_similar(face): 
                return num

        return None

    def merge_intersected(self): 
        for num, face in enumerate(self.faces): 
            for other_num, other_face in enumerate(self.faces[num + 1:]): 
                if face.rect.area() != 0:
                    if Rect.intersection(face.rect, other_face.rect) / float(face.rect.area()) > self.merge_config.same_faces_min_intersection: 
                        self.faces[num].merge(other_face)
                        del self.faces[other_num]




