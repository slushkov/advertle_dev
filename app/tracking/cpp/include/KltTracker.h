#ifndef KLT_TRACKER_H
#define KLT_TRACKER_H

#include <opencv2/opencv.hpp>

#include "Tracker.hpp"

class KltTracker : public Tracker {
public:
    void capture(const cv::Mat &frame, const cv::Rect &captureBox) override;
    TrackerOut track(const cv::Mat &frame) override;

private:
    const float CAPTURE_FRAME_OFFSET = 0.1;
    const int MAX_CORNERS = 100;
    const double QUALITY_LEVEL = 0.0003;
    const double MIN_DISTANCE = 3;
    const int BLOCK_SIZE = 7;

    const cv::Size WIN_SIZE = cv::Size(15, 15);
    const int MAX_LEVEL = 2;

    const double TRACKING_WEIGHT = 0.2;

    const cv::TermCriteria TERM_CRITERIA = cv::TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 10, 0.003);

    cv::Mat oldGrayFrame_;
    cv::Size captureRectSize_;
    cv::Point2f trackPoint_;
    std::vector<cv::Point2f> oldCorners_,
                             currCorners_;
    std::vector<uchar> status_;
    std::vector<float> error_;
    cv::Point2f lastTrackPos_;


    // Track methods //
    int findCorners(const cv::Mat &img, std::vector<cv::Point2f> &corners);
    void calcOpticalFlow(cv::Mat &currFrame, std::vector<cv::Point2f> &currCorners);
    cv::Point2f getResFlowVector(const int &maxError = 550);
    cv::Rect rectAroundPoint(const cv::Point2f &point, const cv::Size &size, const cv::Size &bounds);
    cv::Point2f getAvPoint(const std::vector<cv::Point2f> &points);
};

#endif /* KLT_TRACKER_H */


