#ifndef MULTI_TRACKER_H
#define MULTI_TRACKER_H

#include <vector>
#include <tuple>
#include <opencv2/opencv.hpp>

#include "Tracker.hpp"
#include "KltTracker.h"
#include "TemplateTracker.h"

class MultiTracker : public Tracker
{
public:
    MultiTracker();

    void capture(const cv::Mat &frame, const cv::Rect &capture_box) override;
    TrackerOut track(const cv::Mat &frame) override;

private:
    const float ACCURACY_BORDER_VALUE_ = 0.90;

    std::vector<Tracker*> trackers_;
    cv::Size face_size_;

    static TrackerOut accumulate_trackers(const std::vector<TrackerOut> &outs);
};

#endif /* MULTI_TRACKER_H */
