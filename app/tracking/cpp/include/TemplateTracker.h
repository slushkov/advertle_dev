#ifndef TEMPLATE_TRACKER_H
#define TEMPLATE_TRACKER_H

#include <tuple>
#include <vector>
#include <opencv2/opencv.hpp>

#include "Tracker.hpp"

class TemplateTracker : public Tracker {
public:
    void capture(const cv::Mat &frame, const cv::Rect &rect) override;
    TrackerOut track(const cv::Mat &frame) override;

private:
    const float TRACK_AREA_SCALE_FACTOR_ = 1.33;
    const int TEMPLATE_METHOD_ = cv::TM_SQDIFF_NORMED;
    const float TEMPL_ADJUST_COEFF_ = 0.025;
    const double TRACKING_WEIGHT_ = 1.0;
    const std::vector<float> INCREASE_SCALES_ = { 0.05 };

    cv::Mat obj_templ_;
    cv::Rect curr_area_;

    void increase(cv::Rect &rect);
    void adjust_object_template(const cv::Mat &frame);

    std::tuple<cv::Mat, cv::Point> get_current_detect_area(const cv::Mat &frame);
};

#endif /* TEMPLATE_TRACKER_H */
