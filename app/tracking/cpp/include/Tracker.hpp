#ifndef TRACKER_HPP
#define TRACKER_HPP

#include <tuple>
#include <opencv2/opencv.hpp>

class TrackerOut {
public:
	TrackerOut() = default;
	TrackerOut(const cv::Rect &rect, double confidence): 
		rect(rect), 
		confidence(confidence) {
	}

	bool is_empty() const {
		return rect == cv::Rect() && confidence == 0.0;
	}

	cv::Rect rect; 
	double confidence = 0.0; 
};

class Tracker {
public:
    virtual TrackerOut track(const cv::Mat &frame) = 0;
    virtual void capture(const cv::Mat &frame, const cv::Rect &capture_box) = 0;
};

#endif /* TRACKER_HPP */
