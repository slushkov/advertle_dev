#include "KltTracker.h"

#include "utils.hpp"

void KltTracker::capture(const cv::Mat &frame, const cv::Rect &captureBox) {
    if (frame.type() == CV_8U)
        oldGrayFrame_ = frame.clone();
    else
        cv::cvtColor(frame, oldGrayFrame_, CV_BGR2GRAY);

    trackPoint_ = cv::Point2f(captureBox.x + captureBox.width / 2, captureBox.y + captureBox.height / 2);

    cv::Rect captureRect((1 + CAPTURE_FRAME_OFFSET) * captureBox.x, (1 + CAPTURE_FRAME_OFFSET) * captureBox.y,
                         (1 - 2 * CAPTURE_FRAME_OFFSET) * captureBox.width, (1 - 2 * CAPTURE_FRAME_OFFSET) * captureBox.height);

    captureRectSize_ = captureRect.size();

    if (captureRect.x + captureRect.width > frame.cols)
        captureRect.width = frame.cols - captureRect.x;

    if (captureRect.y + captureRect.height > frame.rows)
        captureRect.height = frame.rows - captureRect.y;

    findCorners(oldGrayFrame_(captureRect), oldCorners_);

    for (cv::Point2f &corner: oldCorners_)
        corner += cv::Point2f(captureBox.x, captureBox.y);
}

TrackerOut KltTracker::track(const cv::Mat &frame) {
    cv::Mat grayFrame;

    if (frame.type() == CV_8U)
        grayFrame = frame.clone();
    else
        cv::cvtColor(frame, grayFrame, CV_BGR2GRAY);

    cv::Rect cornersFindRect     = rectAroundPoint(getAvPoint(oldCorners_), captureRectSize_ * 0.5, frame.size()),
             cornersBoundingRect = rectAroundPoint(getAvPoint(oldCorners_), captureRectSize_ * 1.5, frame.size());

    if (cornersFindRect == cv::Rect())
        return TrackerOut();

    findCorners(grayFrame(cornersFindRect), currCorners_);

    for (auto &corner : currCorners_)
        corner += cv::Point2f(cornersFindRect.x, cornersFindRect.y);

    for (auto it = oldCorners_.begin(); it != oldCorners_.end();)
        it = cornersBoundingRect.contains((*it)) ? ++it : oldCorners_.erase(it);

    calcOpticalFlow(grayFrame, currCorners_);

    cv::Point2f resVector = getResFlowVector();

    oldGrayFrame_ = grayFrame;
    oldCorners_   = currCorners_;

    trackPoint_ += resVector / float(status_.size());

    lastTrackPos_= trackPoint_;

    cv::Point2f res = oldCorners_.size() ? lastTrackPos_ : cv::Point2f();

    cv::Rect ans(cv::Point(res.x - (captureRectSize_.width / 2), res.y - (captureRectSize_.height / 2)),
                 cv::Point(res.x + (captureRectSize_.width / 2), res.y + (captureRectSize_.height / 2)));

    return TrackerOut(ans, 1.0); // TODO : make accuracy
}

int KltTracker::findCorners(const cv::Mat &img, std::vector<cv::Point2f> &corners) {
    goodFeaturesToTrack(img, corners, MAX_CORNERS, QUALITY_LEVEL, MIN_DISTANCE, cv::Mat(), BLOCK_SIZE, true);

    return corners.size();
}

void KltTracker::calcOpticalFlow(cv::Mat &currFrame, std::vector<cv::Point2f> &currCorners) {
    if (!oldCorners_.size() || !currCorners.size() || oldGrayFrame_.empty() || currFrame.empty())
        return;

    calcOpticalFlowPyrLK(oldGrayFrame_, currFrame, oldCorners_, currCorners, status_, error_, WIN_SIZE, MAX_LEVEL, TERM_CRITERIA);
}

cv::Point2f KltTracker::getResFlowVector(const int &maxError) {
    cv::Point2f res;

    for (unsigned int i = 0; i < status_.size(); ++i)
        if (!(status_[i] == 0 || error_[i] > maxError))
            res += currCorners_[i] - oldCorners_[i];

    return res;
}

cv::Rect KltTracker::rectAroundPoint(const cv::Point2f &point, const cv::Size &size, const cv::Size &bounds) {
    cv::Rect rect(point.x - size.width/2, point.y - size.height/2, size.width, size.height);

    if (rect.x < 0) rect.x = 0;
    if (rect.y < 0) rect.y = 0;
    if (rect.x + rect.width  > bounds.width ) rect.width  = bounds.width  - rect.x - 1;
    if (rect.y + rect.height > bounds.height) rect.height = bounds.height - rect.y - 1;

    return rect.width > 0 && rect.height > 0 ? rect : cv::Rect();
}

cv::Point2f KltTracker::getAvPoint(const std::vector<cv::Point2f> &points) {
    cv::Point2f av(0, 0);

    for (cv::Point2f point : points){
        av.x += point.x;
        av.y += point.y;
    }

    av.x /= points.size();
    av.y /= points.size();

    return av;
}
