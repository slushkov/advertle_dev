#define PY_ARRAY_UNIQUE_SYMBOL pbcvt_ARRAY_API
#include <boost/python.hpp>
#include <opencv2/opencv.hpp>

#include "pyboostcvconverter/pyboostcvconverter.hpp"
#include "MultiTracker.h"

namespace py = boost::python;

namespace pbcvt {
//============================ General OpenCV wrappers =====================================//

cv::Rect py_tuple_to_cvRect(py::tuple t) { // NOTE: Alowed tuple rect format: (x, y, w, h)
    return cv::Rect(py::extract<int>(t[0]), py::extract<int>(t[1]), py::extract<int>(t[2]), py::extract<int>(t[3]));
}

//============================ MultiTracker wrappers =====================================//

void MultiTracker_capture(MultiTracker &self, const cv::Mat &frame, py::tuple capture_box) {
    return self.capture(frame, py_tuple_to_cvRect(capture_box));
}

TrackerOut MultiTracker_track(MultiTracker &self, const cv::Mat &frame) {
    return self.track(frame);
}

//============================ TrackerOut wrappers =====================================//

boost::shared_ptr<TrackerOut> TrackerOut_init(py::tuple rect, double confidence) {
    return boost::shared_ptr<TrackerOut>(new TrackerOut(py_tuple_to_cvRect(rect), confidence));
}

py::tuple TrackerOut_get_rect(TrackerOut &self) {
    return py::make_tuple(self.rect.x, self.rect.y, self.rect.width, self.rect.height);
}

void TrackerOut_set_rect(TrackerOut &self, py::tuple rect) {
    self.rect = py_tuple_to_cvRect(rect);
}

//========================================================================================//


static void init_ar(){
    Py_Initialize();
    import_array();
}

BOOST_PYTHON_MODULE(Tracking){
    //using namespace XM;
    init_ar();

    // Initialize converters //
    py::to_python_converter<cv::Mat,
    pbcvt::matToNDArrayBoostConverter>();
    pbcvt::matFromNDArrayBoostConverter();

    // Wraped classes //
    py::class_<MultiTracker>("MultiTracker")
        .def("capture", &MultiTracker_capture)
        .def("track", &MultiTracker_track)
        ;

    py::class_<TrackerOut, boost::shared_ptr<TrackerOut>>("TrackerOut", py::no_init)
        .def("__init__", py::make_constructor(&TrackerOut_init))
        .add_property("rect", &TrackerOut_get_rect, &TrackerOut_set_rect)
        .add_property("confidence", py::make_getter(&TrackerOut::confidence), py::make_setter(&TrackerOut::confidence))
        ;
}

} // namespace pbcvt
