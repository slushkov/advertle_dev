#include <opencv2/opencv.hpp>

#include "TrackingTestApp.hpp"
#include "TemplateTracker.h"


int main() {
	TrackingTestApp<TemplateTracker> app("/home/slushkov/Videos/advertle/afi_with_ivan_2.mp4", cv::Size(1280, 720));

	app.run();

	return 0;
}