#ifndef CAPTURE_APP
#define CAPTURE_APP

#include <string>
#include <queue>
#include <future>
#include <tuple>
#include <iostream>

#include <opencv2/opencv.hpp> 

class CaptureError : public std::exception {
public: 
	CaptureError(const std::string &reason): reason_(reason) {}

	virtual const char* what() const throw() { 
		return (std::string("[Internal] Capture ERROR: ") + this->reason_).c_str();
	}

private: 
	const std::string reason_;
};


class CaptureApp {
public: 
	CaptureApp(int capture_num,               cv::Size resolution): capture_(capture_num), resolution_(resolution) {}
	CaptureApp(const std::string &video_path, cv::Size resolution): capture_(video_path),  resolution_(resolution) {}

	void run() {
		int key = -1; 
		cv::Mat frame; 

		while (key != 'q') {
			bool success = this->capture_.read(frame);

			if (!success) 
				throw CaptureError("Bad (or empty) frame");

			cv::resize(frame, frame, this->resolution_);

			key = cv::waitKey(1);

			this->on_frame(frame, key);
		}
	}

protected: 
	virtual void on_frame(const cv::Mat &frame, int key) = 0; 

private: 
	cv::VideoCapture capture_; 
	cv::Size resolution_; 
};

#endif /* CAPTURE_APP */