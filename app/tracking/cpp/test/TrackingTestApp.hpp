#ifndef TRACKING_TEST_APP 
#define TRACKING_TEST_APP

#include <vector>
#include <opencv2/opencv.hpp> 

#include "CaptureApp.hpp"

template <class TrackerClass> 
class TrackingTestApp : public CaptureApp {
public: 
    template <class CaptureType>
    TrackingTestApp(CaptureType capture, cv::Size resolution): 
    	CaptureApp(capture, resolution) 
    {
		cv::namedWindow(this->WINDOW_NAME_);
        cv::setMouseCallback(this->WINDOW_NAME_, on_mouse, this);
    }

    void on_frame(const cv::Mat &frame, int keyboard_key) override {
        cv::Mat trackers_canvas = frame.clone();

        for (TrackerClass *tracker: this->trackers_) {
            auto result = tracker->track(frame);

            cv::rectangle(trackers_canvas, result.rect, cv::Scalar(0, 0, 255), 2); 
        }

        if (keyboard_key == ' ') {
            this->selection_allowed = true; 

            while (cv::waitKey(1) != ' ') {
                cv::Mat selection_canvas = trackers_canvas.clone();

                if (this->selection_p1 != cv::Point() && this->selection_p2 != cv::Point())
                    cv::rectangle(selection_canvas, this->selection_p1, this->selection_p2, cv::Scalar(255, 0, 0), 2);

                if (this->selection_finished) {
                    this->trackers_.push_back(new TrackerClass());
                    this->trackers_.back()->capture(frame, cv::Rect(this->selection_p1, this->selection_p2));

                    cv::rectangle(trackers_canvas, this->selection_p1, this->selection_p2, cv::Scalar(0, 0, 255), 2);

                    this->selection_finished = false; 
                    this->selection_p1 = cv::Point();
                    this->selection_p2 = cv::Point();
                }
                
                cv::imshow(this->WINDOW_NAME_, selection_canvas);
            }

            this->selection_allowed = false;
        }

        cv::imshow(this->WINDOW_NAME_, trackers_canvas);
    }

protected:  
    cv::Point selection_p1;
    cv::Point selection_p2;
    bool selection_allowed = false;
    bool selection_finished = false;

private: 
    const std::string WINDOW_NAME_ = "Tracking";

    std::vector<TrackerClass*> trackers_; 

    static void on_mouse(int event, int x, int y, int flags, void *user_data) {
		TrackingTestApp<TrackerClass> *self = static_cast<TrackingTestApp<TrackerClass>*>(user_data);

		if (self->selection_allowed) {
		    if (event == cv::EVENT_LBUTTONDOWN) {
		        if (self->selection_p1 != cv::Point())	
		            self->selection_finished = true;
		        else 
		            self->selection_p1 = cv::Point(x, y);
		    }

		    if (self->selection_p1 != cv::Point())
		        self->selection_p2 = cv::Point(x, y);
		}
    }
};

#endif /* TRACKING_TEST_APP */