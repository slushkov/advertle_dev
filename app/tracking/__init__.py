import os
import shutil
import subprocess

__USE_CPP_CODE = True 
SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__)) + "/"

if __USE_CPP_CODE:
    LIB_NAME = "Tracking.so"

    def build(build_verbose=False, verbose=True):
        if verbose:
            print("Please wait, compilation...")

        build_dir = os.path.join(SCRIPT_DIR, "_tmp_build_tracking")

        if os.path.exists(build_dir):
            shutil.rmtree(build_dir)

        os.makedirs(build_dir)

        build_cmds = [
            "cd %s" % (build_dir), 
            "cmake %s" % (os.path.join(SCRIPT_DIR, "cpp/")),
            "make",
            "cp %s %s" % (os.path.join(build_dir, LIB_NAME), SCRIPT_DIR),
            "rm -rf %s" % (build_dir)
        ]

        if not build_verbose:
            build_cmds = [cmd + " > /dev/null" for cmd in build_cmds]

        subprocess.call(" && ".join(build_cmds), shell=True)

        if verbose: 
            print("Compilation done")


    if not os.path.exists(os.path.join(SCRIPT_DIR, LIB_NAME)):
        build()

    from Tracking import *

else: 
    pass
    # TODO: Use py trackers