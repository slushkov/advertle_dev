import numpy as np
import cv2
import sys

sys.path.append("../")

from utils import as_grayscale
from primitives import (Rect, get_roi, Size)


class KltTracker(object):
    FEATURES_PARAMS = dict(maxCorners=100, qualityLevel=0.0003, minDistance=3, blockSize=7)
    LK_PARAMS = dict(winSize=(15, 15), maxLevel=2, criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
    TRACKING_WEIGHT = 0.2

    def capture(self, frame, object_rect):
        self.old_gray = as_grayscale(frame)

        frame_roi = get_roi(self.old_gray, object_rect)
        self.object_size = Size(object_rect.width, object_rect.height)

        offseted_features = cv2.goodFeaturesToTrack(frame_roi, mask=None, **self.FEATURES_PARAMS)
        self.old_features = np.add(offseted_features, np.array([object_rect.x, object_rect.y], dtype=np.float32))
        self.initial_features_count = len(self.old_features)

    def track(self, frame):
        gray = as_grayscale(frame)

        curr_features, status, error = cv2.calcOpticalFlowPyrLK(self.old_gray, gray, self.old_features, None, **self.LK_PARAMS)

        feat_center = self.__features_center_mass(curr_features)

        if feat_center is None:
            return (None, None) 

        curr_obj_rect_tl_x = feat_center[0] - int(self.object_size.width / 2)
        curr_obj_rect_tl_y = feat_center[1] - int(self.object_size.height / 2)

        curr_obj_rect = Rect(curr_obj_rect_tl_x, curr_obj_rect_tl_y, self.object_size.width, self.object_size.height)
        curr_obj_rect = self.__clip_rect_to_frame_shape(curr_obj_rect, frame.shape)

        self.old_gray = gray.copy()
        self.old_features = self.__features_inside_rect(curr_features, curr_obj_rect).astype(np.float32).reshape(-1, 1, 2)

        return (curr_obj_rect, 1.0 - self.TRACKING_WEIGHT * (1.0 - len(curr_features)/float(self.initial_features_count)))

    @staticmethod
    def __clip_rect_to_frame_shape(rect, frame_shape):
        if rect.x < 0: 
            rect.x = 0

        if rect.y < 0:
            rect.y = 0

        rect.width = int(np.clip(rect.x + rect.width, 0, frame_shape[1])) - rect.x
        rect.width = int(np.clip(rect.y + rect.height, 0, frame_shape[0])) - rect.y

        return rect

    def __features_center_mass(self, features):
        if features is None:
            return None

        x_sum = sum([feat[0][0] for feat in features])
        y_sum = sum([feat[0][1] for feat in features])
        features_count = len(features)

        return (int(x_sum / features_count), int(y_sum / features_count))

    def __features_inside_rect(self, features, rect):
        features_inside = []

        for feat in features:
            if rect.is_inside((feat[0][0], feat[0][1])):
                features_inside.append((feat[0][0], feat[0][1]))

        result = np.ndarray(shape=(len(features_inside), 1, 2))

        for num, point in enumerate(features_inside):
            result[num][0][0], result[num][0][1] = point

        return result

    def __features_bounding_rect(self, features):
        tl = [-1, -1]
        br = [-1, -1]

        for feat in features:
            x = feat[0][0]
            y = feat[0][1]

            if x < tl[0] or tl[0] == -1:
                tl[0] = x
            if y < tl[1] or tl[1] == -1:
                tl[1] = y
            if x > br[0] or br[0] == -1:
                br[0] = x
            if y > br[1] or br[1] == -1:
                br[1] = y

        return Rect(tl[0], tl[1], br[0] - tl[0], br[1] - tl[1])   # (x, y, w, h)
