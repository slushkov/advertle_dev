import sys
import numpy as np
import cv2
import copy

sys.path.append("../")

from primitives import (Rect, get_roi)


class TemplateTracker:
    TRACK_AREA_SCALE_FACTOR = 2.0
    TEMPLATE_METHOD = cv2.TM_SQDIFF_NORMED
    TEMPL_ADJUST_COEFF = 0.025
    TRACKING_WEIGHT = 1.0
    INCREASE_SCALES = [0.05]

    def capture(self, frame, rect):
        '''
        this method get track object for future tracking
        @frame (numpy.ndarray) : picture of tracking object
        @rect (primitives.Rect): coordinates of tracking object on main frame
        @return () : None
        '''

        self.curr_area = rect
        self.obj_templ = get_roi(frame, rect)

    def track(self, frame):
        '''
        this method track object on new frame
        @frame (numpy.ndarray) : new frame
        @return (primitives.Rect, float) : new coordinates of tracking object, track accuracy
        '''

        tracking_area_frame, track_area_tl = self.__get_current_detect_area(frame)

        track_area = Rect(track_area_tl[0], track_area_tl[1], tracking_area_frame.shape[1], tracking_area_frame.shape[0])

        object_frames = [self.obj_templ]

        for scale in self.INCREASE_SCALES:
            object_frames.append(cv2.resize(self.obj_templ, (0, 0), fx=(1.0 + scale), fy=(1.0 + scale)))
            object_frames.append(cv2.resize(self.obj_templ, (0, 0), fx=(1.0 - scale), fy=(1.0 - scale)))

        match_results = [cv2.matchTemplate(tracking_area_frame, obj_frame.astype(np.uint8), self.TEMPLATE_METHOD) for obj_frame in object_frames]
        min_max_values = [cv2.minMaxLoc(match) for match in match_results]
        min_vals = [min_max[0] for min_max in min_max_values]

        best_area_index = min_vals.index(min(min_vals))

        best_val, _, (best_x, best_y), _ = min_max_values[best_area_index]


        self.obj_templ = object_frames[best_area_index]
        self.curr_area = Rect(track_area.x + best_x, track_area.y + best_y, self.obj_templ.shape[1], self.obj_templ.shape[0])

        self.__adjust_object_template(frame)

        return (self.curr_area, (1.0 - best_val) * self.TRACKING_WEIGHT)

    def __adjust_object_template(self, frame): 
        self.obj_templ = (1.0 - self.TEMPL_ADJUST_COEFF) * self.obj_templ + self.TEMPL_ADJUST_COEFF * get_roi(frame, self.curr_area)

    def __get_current_detect_area(self, frame):
        search_area = copy.copy(self.curr_area)
        search_area.increase(self.TRACK_AREA_SCALE_FACTOR)
        search_area.clip_to_area(frame.shape[1], frame.shape[0])

        return get_roi(frame, search_area), (search_area.x, search_area.y)
