import sys
from primitives import Rect

__USE_CPP_TRACKER__ = True

if __USE_CPP_TRACKER__:
    import Track

    class MultiTracker(object):
        def __init__(self) :
            self.tracker = Track.Tracker()

        def capture(self, frame, object_rect):
            rect = object_rect.to_tuple()
            self.tracker.capture(frame, float(rect[0]), float(rect[1]), float(rect[2]), float(rect[3]))

        def track(self, frame):
            track_res = self.tracker.track(frame)

            if track_res[-1] < 0.0 : 
            	return None

            return (Rect(*track_res[:-1]), track_res[-1])

else: 
    sys.path.append("../")

    from utils import vec_diff_len

    from .klt_tracker import KltTracker
    from .template_tracker import TemplateTracker

    class MultiTracker(object):
        ACCURACY_BORDER_VALUE = 0.90

        def __init__(self):
            self.trackers = [TemplateTracker()]

        def capture(self, frame, object_rect):
            self.face_size = (object_rect.width, object_rect.height)

            for tracker in self.trackers:
                tracker.capture(frame, object_rect)

        def __is_box_near_border(self, box, frame_shape, coeff=0.15):
            return box.x < frame_shape[1] * coeff or box.y < frame_shape[0] * coeff or box.br()[0] > (1.0 - coeff) * frame_shape[1] or box.br()[1] > (1.0 - coeff) * frame_shape[0]

        def __recapture_if_possible(self, tracker_num, track_results, frame):
            true_detects = list(track_results)
            del true_detects[tracker_num]

            capture_area = self.__track_area_around_point(self.__get_av_point(true_detects))

            if not self.__is_box_near_border(capture_area, frame.shape):
                self.trackers[tracker_num].capture(frame, capture_area)
                track_results[tracker_num] = self.trackers[tracker_num].track(frame)[0]
                return True

            return False

        def track(self, frame):
            results = [tracker.track(frame) for tracker in self.trackers]

            track_rects = [res[0] for res in results]
            track_values = [res[1] for res in results]

            self.face_size = (results[0][0].width, results[0][0].height)  # NOTE : it's only for TemplateUsing

            for tracker_num in xrange(len(results)):
                if track_values[tracker_num] is None or track_values[tracker_num] < self.ACCURACY_BORDER_VALUE:
                    recapture_success = self.__recapture_if_possible(tracker_num, track_rects, frame)
                    if not recapture_success:
                        return None

            return (self.__track_area_around_point(self.__get_av_point(track_rects)), 0.5) # TODO: Multi tracker accuracy

        def __track_area_around_point(self, track_point):
            return Rect(track_point[0] - self.face_size[0] / 2, track_point[1] - self.face_size[1] / 2, self.face_size[0], self.face_size[1])

        @staticmethod
        def __get_av_point(rects):
            av_point = (-1, -1)

            for rect in rects:
                if av_point == (-1, -1):
                    av_point = rect.center_point()
                else:
                    rect_central = rect.center_point()

                    x = int((av_point[0] + rect_central[0]) / 2)
                    y = int((av_point[1] + rect_central[1]) / 2)

                    av_point = (x, y)

            return av_point









