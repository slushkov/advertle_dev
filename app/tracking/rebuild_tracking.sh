BUILD_DIR="_tmp_trakcing_build"

mkdir $BUILD_DIR
cd $BUILD_DIR
cmake -DCMAKE_BUILD_TYPE=Debug ../cpp/
make 
mv Tracking.so ../
cd ..
rm -rf $BUILD_DIR