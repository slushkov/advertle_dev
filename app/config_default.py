import cv2

RESOURCES_FOLDER = "app/resources"
RESOLUTION = "HD"

FACES_DETECT = dict(
	cascade_file="haarcascade_frontalface_alt.xml", 
	detect_params=dict(scaleFactor=1.2, minNeighbors=5, minSize=(30, 30), maxSize=(80, 80)),
	detect_period=10, 
	enable=True
)

TRACKING = dict(
	enable=True
)

RECOGNITION = dict(
	classifiers={
		"gender": dict(
			struct="gender.prototxt", 
			model="gender.caffemodel", 
			mean="gender_mean.binaryproto", 
			input_shape=(1, 1, 50, 50),
			transform=lambda img: cv2.cvtColor(img, cv2.COLOR_BGR2GRAY),
			labels={
				0: "female", 
				1: "male"
			}
		)
		# , 
		# "age": dict(
		# 	struct="age.prototxt",    
		# 	model="age.caffemodel",    
		# 	input_shape=(1, 1, 50, 50),
		# 	transform=lambda img: cv2.cvtColor(img, cv2.COLOR_BGR2GRAY),
		# 	labels={
		# 		0: "15-20", 
		# 		1: "20-25", 
		# 		2: "25-30",
		# 		3: "30-40", 
		# 		4: "40-50", 
		# 		5: "50-60"
		# 	}
		# )
	},
	enable=True
)

FACES_MERGE = dict(
	same_faces_min_intersection=0.4,
	enable=True
)

STATISTICS = dict(
	file="_tmp_statistics.json",
	enable=False
)

GUI = dict(
	enable=True
)










