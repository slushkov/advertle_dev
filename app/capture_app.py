import cv2

class CaptureApp(object): 
	def __init__(self, capture, resize_to=None): 
		self.__capture = cv2.VideoCapture(capture)
		self.__resize_to = resize_to

	def on_frame(self, frame): 
		pass

	def run(self): 
		while True: 
			ret, frame = self.__capture.read() 

			if not ret: 
				raise exceptions.CaptureError("Captured empty frame")

			if self.__resize_to is not None: 
				frame = cv2.resize(frame, self.__resize_to)

			self.on_frame(frame)
