import cv2
import datetime
import numpy as np

import tracking 
from utils import as_grayscale, vec_diff_len
from primitives import get_roi, Rect
import recognize.labels as labels

class Face(object):
    SIMILAR_FACES_AREAS_MIN_RELATION = 0.6
    SIMILAR_FACES_DIST_COEFF = 0.8
    SIMILAR_FACES_MIN_CORR = 0.85

    def __init__(self, frame, rect): 
        self.rect = rect
        self.photo = get_roi(frame, rect) # TODO
        self.appear_time = datetime.datetime.now()
        self.status = "captured"

        self.predictions = {}

        self.tracker = tracking.MultiTracker()
        self.tracker.capture(frame, rect.to_tuple())

    def stat(self):
        predictions = {param_name: labels.NAMES[param_name][self.__accumulate_hisory(predicts)] for param_name, predicts in self.predictions.items()}

        return {
            "appear_time": self.appear_time.strftime("%H:%M:%S"), 
            "loss time": "11:03", # TODO
            "rect": (self.rect.tl(), self.rect.br()),
            "predictions": predictions
        }

    def merge(self, other): 
        for predict_name in other.predictions: 
            self.predictions[predict_name] += other.predictions[predict_name]
        
        self.appear_time = min(self.appear_time, other.appear_time)

    def track(self, frame): 
        track_result = self.tracker.track(frame)

        if track_result is None: 
            self.status = "falled"
        else: 
            self.rect = Rect(*track_result.rect)
            self.status = "captured"

    def update(self, other):
        self.photo = other.photo 
        self.rect = other.rect
        self.status = "captured"

        # Tracker recapture? 

    def is_similar(self, other, show=False): 
        if self.rect.area() / float(other.rect.area()) < self.SIMILAR_FACES_AREAS_MIN_RELATION:
            if show: 
                print("Not same faces: area ({})".format(self.rect.area() / other.rect.area()))

            return False

        tl_diff = vec_diff_len(self.rect.tl(), other.rect.tl())
        br_diff = vec_diff_len(self.rect.br(), other.rect.br())

        max_distance = self.rect.diagonal_len() * self.SIMILAR_FACES_DIST_COEFF

        if (tl_diff + br_diff) > max_distance:
            if show: 
                print("Not same faces: distance")


            return False

        match_val = cv2.matchTemplate(self.photo, cv2.resize(other.photo, (self.photo.shape[1], self.photo.shape[0])), cv2.TM_CCORR_NORMED)

        if match_val[0][0] < self.SIMILAR_FACES_MIN_CORR:
            if show: 
                print("Not same faces: corr value")

            return False

        if show: 
            cv2.imshow("Face 1", self.photo)
            cv2.imshow("Face 2", other.photo)
            cv2.waitKey()

            print("Same faces")

        return True

    def append_predictions(self, predictions): 
        for param_name, predict in predictions.items():
            if not param_name in self.predictions: 
                self.predictions[param_name] = [predict]
            else:
                self.predictions[param_name].append(predict)
                
    @staticmethod
    def __accumulate_hisory(history): 
        accumulated = [sum(vals) for vals in zip(*history)]
        
        return np.array(accumulated).argmax()