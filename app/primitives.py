import numpy as np

class Rect(object): 

    def __init__(self, x, y, width, height): 
        self.x = int(x)
        self.y = int(y)
        self.width = int(width)
        self.height = int(height)

    def __repr__(self):
        return "<Rect x: %d y: %d width: %d height: %d>" % (self.x, self.y, self.width, self.height)

    def to_tuple(self):
        return (self.x, self.y, self.width, self.height) 

    @classmethod
    def intersection(cls, rect1, rect2):
        max_tl_x = max(rect1.tl()[0], rect2.tl()[0]);
        max_tl_y = max(rect1.tl()[1], rect2.tl()[1]);
        min_br_x = min(rect1.br()[0], rect2.br()[0]);
        min_br_y = min(rect1.br()[1], rect2.br()[1]);

        if max_tl_x >= min_br_x or max_tl_y >= min_br_y: 
            return 0 
            
        return abs(max_tl_x - min_br_x) * abs(max_tl_y - min_br_y)

    def center_point(self):
        return (int((self.x + (self.x + self.width))/2), int((self.y + (self.y + self.height))/2))

    def br(self):
        return (self.x + self.width, self.y + self.height)

    def tl(self):
        return (self.x, self.y) 

    def diagonal_len(self):
        return (self.width ** 2 + self.height ** 2) ** 0.5

    def area(self):
        return self.width * self.height

    def clip_to_area (self, area_width, area_height):
        self.x = np.clip(self.x, 0, area_width)
        self.y = np.clip(self.y, 0, area_height)

        if self.x + self.width > area_width:
            self.width = area_width - self.x

        if self.y + self.height > area_height:
            self.height = area_height - self.y

    def increase(self, scale):
        size_scale = 1 + 2.0 / scale

        self.x -= int(self.width / scale)
        self.y -= int(self.height / scale)

        self.width *= size_scale
        self.height *= size_scale


def get_roi(frame, rect):
    return frame[rect.y : rect.y + rect.height, rect.x : rect.x + rect.width]

class Size(object):
    def __init__(self, width, height):
        self.width = width
        self.height = height
