import os

__in_script_folder = lambda name : os.path.join(os.path.abspath(os.path.dirname(__file__)), name)

RESOURCES_PATH = __in_script_folder("resources/")

FACE_CASCADE_FILE_NAME = "haarcascade_frontalface_alt.xml"
GENDER_CNN_FILES = ("gender.caffemodel", "gender.prototxt", "gender_mean.binaryproto")
AGE_CNN_FILES = ("age.caffemodel", "age.prototxt")

# Join with resources path

FACE_CASCADE_FILE_NAME = os.path.join(RESOURCES_PATH, FACE_CASCADE_FILE_NAME)
GENDER_CNN_FILES = tuple([os.path.join(RESOURCES_PATH, path) for path in GENDER_CNN_FILES])
AGE_CNN_FILES = tuple([os.path.join(RESOURCES_PATH, path) for path in AGE_CNN_FILES])