import json

from faces_base import FacesBase
from faces_detector import FacesDetector
from recognize.classifier import MultiClassifier
from capture_app import CaptureApp
import frontend.frontend as frontend
import config_parser
import exceptions


class App(CaptureApp):
    def __init__(self, capture, user_config=None):
        self.config = config_parser.parse(user_config)

        super(App, self).__init__(capture, self.config.resolution)

        self.faces_base = FacesBase(self.config.faces_merge)
        self.face_detector = FacesDetector(self.config.faces_detect, self.faces_base)
        self.recognizer = MultiClassifier(self.config.recognition)


    def on_frame(self, frame):
        self.face_detector.detect_to_base(frame)

        if self.config.tracking.enable: 
            self.faces_base.track_active(frame)

        if self.config.recognition.enable:
            self.__recognize_faces()

        if self.config.statistics.enable: 
            json.dump(self.faces_base.stat(), open(self.config.statistics.file, "w"))

        if self.config.gui.enable:
            frontend.show(frame, self.faces_base.stat())


    def __recognize_faces(self):
        self.faces_base.foreach(("captured", ), lambda face: face.append_predictions(self.recognizer.recognize(face.photo)))