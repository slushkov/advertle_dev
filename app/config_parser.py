import os
import imp
import collections

import exceptions

BASE_CONFIG_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "config_default.py")

SUPPORTED_RESOLUTIONS = {
    "HD":     (1280, 720), 
    "FullHD": (1920, 1080)
}


def parse(user_config_path=None):
    values = get_config_values(BASE_CONFIG_PATH)

    if user_config_path is not None: 
        user_values = get_config_values(user_config_path)
        values = update_config_values(values, user_values)

    return preprocess_values(values)


def get_config_values(file_path): 
    config = imp.load_source("config", file_path)
    return {key: value for key, value in config.__dict__.iteritems() if key[:2] != "__"} 


def update_config_values(origin_dict, upd_dict):
    for upd_key, upd_value in upd_dict.iteritems():
        if isinstance(upd_value, collections.Mapping):
            origin_dict[upd_key] = update_config_values(origin_dict.get(upd_key, {}), upd_value)
        else:
            origin_dict[upd_key] = upd_dict[upd_key]

    return origin_dict


def preprocess_values(values):
    '''
    Chack and preprocess raw values from config: 
    - RESOLUTION: HD -> (1280, 720)
    - RESOURCES_FOLDER: to absolute path 
    - All dicts -> obejcts (for usefull access by . (Example: config["TRACKING"]["enable"] -> config.tracking.enable))

    @arg values <dict> Raw data from config

    @ret <class 'config'> Class with preprocessed config values
    '''

    check_required_values_existance(values)
    
    for value_name in ("RESOLUTION", "RESOURCES_FOLDER", "FACES_DETECT", "TRACKING", "RECOGNITION", "FACES_MERGE", "STATISTICS", "GUI"):
        values[value_name.lower()] = values.pop(value_name)

    values["resolution"] = preprocess_resolution(values["resolution"])
    values["resources_folder"] = preprocess_resources_folder(values["resources_folder"])

    values["faces_detect"]["cascade_file"] = os.path.join(values["resources_folder"], values["faces_detect"]["cascade_file"])

    if "classifiers" in values["recognition"]:
        for classifier_name, params in values["recognition"]["classifiers"].iteritems():
            for required_value in ("model", "struct", "labels"): 
                if not required_value in params: 
                    raise exceptions.ConfigError("Missed value '%s' for classifier '%s'" % (required_value, classifier_name))

            abs_paths = {
                "model":  os.path.join(values["resources_folder"], params["model"]), 
                "struct": os.path.join(values["resources_folder"], params["struct"]) 
            }

            if "mean" in params:
                abs_paths["mean"] = os.path.join(values["resources_folder"], params["mean"])

            for path_name in abs_paths: 
                if not os.path.exists(abs_paths[path_name]): 
                    raise exceptions.ConfigError("Classifier '%s' file '%s' is not exists (%s)" % (classifier_name, path_name, abs_paths[path_name]))

            values["recognition"]["classifiers"][classifier_name].update(abs_paths)


    for block_name in ("faces_detect", "tracking", "recognition", "faces_merge", "statistics", "gui"):
        values[block_name] = type(block_name, (object,), values[block_name])

    return type("config", (object,), values)


def check_required_values_existance(values):
    check_in_dict(values, ("RESOLUTION", "RESOURCES_FOLDER", "FACES_DETECT", "TRACKING", "RECOGNITION", "FACES_MERGE", "STATISTICS", "GUI"), 
        lambda value_name: exceptions.ConfigDefaultsError(value_name))

    check_in_dict(values["FACES_DETECT"], ("enable", "cascade_file", "detect_params", "detect_period"), 
        lambda value_name: exceptions.ConfigDefaultsError("faces_detect." + value_name))

    check_in_dict(values["TRACKING"], ("enable",), 
        lambda value_name: exceptions.ConfigDefaultsError("tracking." + value_name))

    check_in_dict(values["RECOGNITION"], ("enable",), 
        lambda value_name: exceptions.ConfigDefaultsError("recognition." + value_name))

    check_in_dict(values["FACES_MERGE"], ("enable", "same_faces_min_intersection"), 
        lambda value_name: exceptions.ConfigDefaultsError("faces_merge." + value_name))

    check_in_dict(values["STATISTICS"], ("enable", "file"), 
        lambda value_name: exceptions.ConfigDefaultsError("statistics." + value_name))

    check_in_dict(values["STATISTICS"], ("enable",), 
        lambda value_name: exceptions.ConfigDefaultsError("gui." + value_name))


def check_in_dict(check_in, required_keys, exception_creator):
    for key in required_keys:
        if not key in check_in:
            raise exception_creator(key)

def preprocess_resolution(value):
    try: 
        return SUPPORTED_RESOLUTIONS[value]
    except:
        raise exceptions.ConfigError("Unknown resolution: '%s'" % value)

def preprocess_resources_folder(value): 
    value = os.path.normpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", value))

    if not os.path.exists(value): 
        raise exceptions.ConfigError("Resources folder does not exists")  

    return value
