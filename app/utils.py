import cv2

def as_grayscale(frame):
    if len(frame.shape) == 3:
        return cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    return frame

def vec_diff_len(p1, p2):
    assert(len(p1) == len(p2))
    return sum([(p1[dim] - p2[dim]) ** 2 for dim in xrange(len(p1))]) ** 0.5
