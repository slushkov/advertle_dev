import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../"))

from net import Net
import paths

class MultiClassifier(object):
	def __init__(self, config): 
		self.classifiers = {name: Net(**params) for name, params in config.classifiers.iteritems()}

	def recognize(self, frame, enable_gender_crutch=True):
		result = {}

		for classif_name, net in self.classifiers.items():
			net_out = list(net.predict(frame)[0]) # NOTE: if we will not convert np.array -> list we will have history accum problems

			if enable_gender_crutch: 
				if classif_name == "gender": 
					if net_out[0] > 0.999: 
						net_out = [0, 1.0]
						print("Used crutch")

			result[classif_name] = net_out # Index of max value on softmax layer

		return result