Caffe didn't support Python3 (you can build it with python3 but will have some runtime errors). Because of this we use Python 2.7 in prediction module. 
Maybe will be possible to add Python3 backward compatibility module in app for comfortable usage of prediction module.
