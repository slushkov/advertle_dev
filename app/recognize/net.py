import os
import cv2
import skimage
import numpy as np

os.environ["GLOG_minloglevel"] = "2" # Caffe quiet (log levels: 0 - debug, 1 - all info, 2 - warnings, 3 - errors)
import caffe
caffe.set_mode_cpu()


class Net(object):
    def __init__(self, model, struct, input_shape, mean=None, transform=None, labels=None):
        self.input_shape = input_shape
        self.image_transform=transform
        self.labels = labels # Remove this

        self.net = caffe.Net(struct, model, caffe.TEST)
        self.net.blobs["data"].reshape(*(input_shape))

        self.transformer = caffe.io.Transformer({"data": self.net.blobs["data"].data.shape})
        self.transformer.set_transpose("data", (2, 0, 1))
        self.transformer.set_raw_scale("data", 255)

        if input_shape[1] != 1:
            self.transformer.set_channel_swap("data", (2, 1, 0))

        if mean is not None:
            blob = caffe.proto.caffe_pb2.BlobProto()

            with open(mean, "rb") as f:
                blob.ParseFromString(f.read())

            data = np.array(caffe.io.blobproto_to_array(blob))[0]

            self.transformer.set_mean("data", data)


    def predict(self, img):
        img = self.image_transform(img)
        img = cv2.resize(img, self.input_shape[-2:])
        img.reshape(self.input_shape)

        self.net.blobs["data"].data[...] = [self.transformer.preprocess("data", self.__skimg_form_cv(img))]

        return self.net.forward()["loss"]


    @staticmethod
    def __skimg_form_cv(img):
        skImg = skimage.img_as_float(img).astype(np.float32)

        if skImg.ndim == 2:
            skImg = skImg[:, :, np.newaxis]

            if len(img.shape) != 2:  # color image case
                skImg = np.tile(skImg, (1, 1, 3))

        elif skImg.shape[2] == 4:
            skImg = skImg[:, :, :3]

        return skImg
