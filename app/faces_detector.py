import cv2
import sys
import os

sys.path.append(os.path.dirname(os.path.abspath(__file__)))

import paths
from utils import as_grayscale
from primitives import Rect
from face import Face

class FacesDetector(object): 
    def __init__(self, config, faces_base=None): 
        self.config = config
        self.faces_base = faces_base
        
        self.face_cascade = cv2.CascadeClassifier()
        ret = self.face_cascade.load(config.cascade_file)

        if not ret: 
            raise Exception("Couldn't open cascade from file '{}'".format(config.cascade_file))

        self.frame_num = -1

    def detect_to_base(self, frame):
        self.frame_num += 1

        if self.frame_num % self.config.detect_period != 0: 
            return

        detects = self.find_faces(frame)

        for rect in (Rect(*d) for d in detects):
            self.faces_base.add_or_update_face(Face(frame, rect))

        if self.faces_base.merge_config.enable: 
            self.faces_base.merge_intersected()


    def find_faces(self, frame):
        return self.face_cascade.detectMultiScale(as_grayscale(frame), **self.config.detect_params) # TODO: as_grayscale




